WP=$(cd $(dirname $0); pwd)
echo "run php"
docker run --rm -it \
        -v ${WP}:/app \
        -w /app \
        php:7-cli  \
	php  ./test.php
