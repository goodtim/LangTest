package main

import "fmt"
import "time"
import "os"
import "strconv"
func runs(val int64)(int64,int64){

	var count int64
	var i int64
	count = 0
	for i = 0; i < val; i++ {
		count =count+ i
	}
	return i,count
}
func main() {
	var val int64
	val = int64(1000000000)
	if len(os.Args) >1 {
		val,_ = strconv.ParseInt(os.Args[1], 10, 64)
	}
	t1 := time.Now() //记录开始运行时间
	i,count := runs(val)


	t2 := time.Now()
	t3 := t2.Sub(t1).Seconds()
	fmt.Printf("end i:%d count:%d time:%f \n", i, count, t3)

}
