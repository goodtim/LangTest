<?php
//PHP是世界最好的语言
function run($val){
	$i=0;
	$count=0;
	for(;$i<$val;$i++)
		$count+=$i;
	return [$i,$count];
}
$stime=microtime(true); //获取程序开始执行的时间
list($i,$count) = run(1000000000);
$etime=microtime(true);//获取程序执行结束的时间
$total=$etime-$stime;   //计算差值
echo "end i:{$i} count:{$count} time:{$total}\n";
