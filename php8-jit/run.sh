WP=$(cd $(dirname $0); pwd)
echo "run php8-jit:"
docker run --rm -it \
        -v ${WP}:/app \
        -w /app \
        php8-jit  \
	php  -dopcache.enable=1 \
            -dopcache.jit_buffer_size=64m \
            -dopcache.jit=1205 \
            -dopcache.file_cache=1 \
            -dopcache.jit_hot_loop=0 \
            -dopcache.enable_cli=1 \
            /app/test.php
