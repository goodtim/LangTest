#说明#

只用于测试不同语言间循环相关的情况,测试不完成.不一定准确.只是用于验证我心中的问题.

# 环境 #
 我测试的环境为Deepin 
 * cpu:8u
 * 内存:14G
 * gcc:9.4.0
 * go:1.10
 * php:7.4
 * php:8.1-jit
 * java:11.5
 * python:3.10
 * pypy:3.4

# 测试结果 #
    run C:
    end i:1000000000 count 499999999500000000 time:0.711511
    run go:
    end i:1000000000 count:499999999500000000 time:0.388051 
    run java:
    end i:1000000000 count:499999999500000000 time:0.423
    run php8-jit:
    end i:1000000000 count:499999999500000000 time:2.5934200286865
    run php
    end i:1000000000 count:499999999500000000 time:11.780023813248
    run nodejs:
    end i:1000000000 count:499999999500000000 time:5.95s
    run pypy:
    end i:1000000000 count:499999999500000000 time:1.0975966453552246
    run python:
    end i:1000000000 count:499999999500000000 time:145.05391883850098



# 排名分别是: #

    1. golang
    2. java
    3. c
    4. php8-jit
    5. pypy
    6. nodejs
    7. php
    8. python




# 结论 #

1.根据java和nodejs得出:使用JIT的语言明显速度加快.所以动态语言和静态语言没有显明的优势差别.

2.golang和c在没有优化时,明显不足于java和nodejs,但如果启用优化,明显可超过jit.但jit的实际使用意义更大.因为在实际总算法会更复杂.所以有动态优化的话,会得到更好的结果.

3,php8已经明确增加jit,所以php8会是一个值得期待的版本.

4,python2.7超出想像的慢,但pypy(with JIT)直接超过没有优化的golang和C.






# 调整 #

    * 2020-04-24 网友 @道一声凉秋 建议C和 go的优化本身也是他的语言特性,所以不应该去掉.所以使用了 @少年你还不懂事 建议,改为输入值,就不会被优化了.
    * 2020-04-24 nodejs 计算值错误,经 @依剑_听雨 提醒改为使用 BigInt, 但速度立即变为148s, 尝试使用 node 14 .但没有明变的改好.所以使用nodejs计算大数时要小心了.
    # 2020-04-27 网友 @hell0cat 调整了nodejs/test.js的算法.确实比直接使用BigInt要强很多.
    # 2020-04-27 增加php8-jit 运算.php8-jit,nodejs,pypy三大脚本语言基本接近
    # 2021-04-02 将php8改为使用最新的官网镜像.效率比原来的差得太远了.一心以为php8可以超过nodejs
    # 2022-07-26 php,php8-jit,python,pypy,nodejs改为使用docker运行，并升级部分版本。php8已经超过nodejs.python真得太慢。c语言比java和go都慢，这不合理，但可能是我不懂C。
