# -*- coding: UTF-8 -*-
# Filename : test.py
# author by : Tim.Huang
import time
def runs(val):
    i=0
    count=0
    while i<val:
        count+=i
        i+=1
    return count

val=1000000000
start = time.time()
count = runs(val)
end = time.time()
print('end i:{} count:{} time:{}'.format(val,count,str(end-start)))
