// nodejs比php快很多，因为js原生只能支持2的53次方整数，而非其他语言的64位整形，所以无法原生支持超过这个范围的整数运算
// 但是用了BigInt，而根据V8团队的博客(https://v8.dev/blog/bigint#optimization-considerations)，BigInt没有做任何的计算优化，因为这不是他们目前考虑的点，所以才会变得非常慢，
// 他们要根据后续的使用反馈，来决定是否针对BigInt做优化，而目前的BigInt实现就是一堆数字数组，当然慢。
// we first want to see how you will use BigInts, so that we can then optimize precisely the cases you care about!
// 用js简单写一个Long类型，只实现加法，实现原理是，如果超过了，就扩充数组，最后把所有数组原素的值转换成BigInt相加得最终结果。
// 实际在我本地测试耗时4s，而php版本的耗时14s。建议去掉“所以使用nodejs计算大数时要小心了”，因为node的V8引擎速度根本不慢
function Long(n) {
  let val = [n];
  let idx = 0;
  function add(v) {
    val[idx] + v < Number.MAX_SAFE_INTEGER ? (val[idx] += v) : (val.push(v), idx++);
  }

  return { add, toString: () => val.reduce((s, v) => s + BigInt(v), 0n).toString() };
}
function runs(val){
  let count = new Long(0);
  let i = 0;
  for (; i < val; i++) {
    count.add(i);
  }
  return {i:i,count:count}

}

(() => {
  let stime = new Date();
  let val = 1000000000;
  var rs = runs(val);
  let etime = new Date();
  let time = (etime.getTime() - stime.getTime()) / 1000;
  let i = rs.i;
  let count = rs.count;
  console.log(`end i:${i} count:${count} time:${time}s`);
})();
